#!/bin/bash

# Begin Initial Server Configuration Setup
sudo yum update -y
sudo yum install -y httpd24 gcc git mod24_ssl
sudo yum install -y mysql57 mysql57-server
sudo yum install -y php php-cli php-devel php-fpm php-gd php-imap php-json php-mbstring php-mcrypt php-mysqlnd php-opcache php-pdo php-pecl-apcu php-xml php-xmlrpc
sudo service httpd start
sudo chkconfig httpd on
chkconfig --list httpd
sudo usermod -a -G apache ec2-user
sudo service httpd restart
sudo chown -R ec2-user:apache /var/www
sudo chmod 2775 /var/www
find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;
sudo yum groupinstall "Development Tools" -y
sudo yum-config-manager --enable epel
sudo service httpd restart
sudo service mysqld start --explicit_defaults_for_timestamp
mysqladmin -u root password 'm1y2q6g9'
sudo chkconfig mysqld on
mysql -u root -p"m1y2q6g9" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
mysql -u root -p"m1y2q6g9" -e "DELETE FROM mysql.user WHERE User=''"
mysql -u root -p"m1y2q6g9" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
mysql -u root -p"m1y2q6g9" -e "FLUSH PRIVILEGES"
sudo service httpd restart
# End Initial Server Configuration Setup

# Ruby and RVM Installation
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable --ruby
source /home/ec2-user/.rvm/scripts/rvm
rvm install "ruby-2.5.0"
source /home/ec2-user/.rvm/scripts/rvm
rvm --default use ruby-2.5.0
# End Ruby and RVM Installation


cd /var/www/html
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
cd ~/
touch .bash_profile
nano .bash_profile # Add the next line to bottom
export PATH="$HOME/.composer/vendor/bin:$PATH"
cd /var/www/html
composer global require drush/drush:7.*
composer global update

# Setup SSL via Certbot
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
sudo ./certbot-auto --debug -v --server https://acme-v01.api.letsencrypt.org/directory certonly -d robertblack.me -d www.robertblack.me

sudo nano /etc/httpd/conf.d/ssl.conf

CERTIFICATE PATHS
	/etc/letsencrypt/live/davidmathewsdevelopment.com/cert.pem - Certificate
	/etc/letsencrypt/live/davidmathewsdevelopment.com/fullchain.pem - Full Chain
	/etc/letsencrypt/live/davidmathewsdevelopment.com/privkey.pem - Private Key

sudo service httpd restart

sudo nano /etc/crontab
39      1,13    *       *       *       root    /home/ec2-user/certbot-auto renew --no-self-upgrade --debug

sudo service crond restart
# End Certbot Setup -- Exit and Reconnect via SSH.

# Nodejs and NVM Installation -- `nvm install --lts` will install the latest version
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 11.13.0
nvm alias default v11.13.0
# End Nodejs and NVM Installation

# Begin PECL Uploadprogress Installation
sudo su
cd ~/
git clone https://git.php.net/repository/pecl/php/uploadprogress.git
cd uploadprogress
/usr/bin/phpize
./configure
make
sudo make install

vi /etc/php.d/20-uploadprogress.so
# ADD THE FOLLOWING TO END OF FILE
; Enable Upload Progress
extension=uploadprogress.so

apachectl -t
apachectl restart

vi /etc/php.ini
# ADD THE FOLLOWING TO END OF FILE
extension=uploadprogress.so

sudo apachectl restart
# End PECL Uploadprogress Installation

# CHANGE `ALLOW OVERRIDE` TO ALL in `var/www/html` FOR CLEAN URLS TO WORK
sudo vim /etc/httpd/conf/httpd.conf
sudo service httpd restart
# Exit and Reconnect via SSH.

# Begin Drupal 7 Installation and Configuration
cd /var/www/html
drush dl drupal-7.59
mv drupal-7.59/* drupal-7.59/.htaccess ./
mv drupal-7.59/.gitignore ./
mv drupal-7.59/.editorconfig ./
rm -rf drupal-7.59
cp sites/default/default.settings.php sites/default/settings.php
chmod a+w sites/default/settings.php
chmod a+w sites/default
mkdir sites/all/modules/contrib
mkdir sites/all/modules/custom
mkdir tmp
drush si --site-name="David Mathews Development" --account-name=robert --account-pass=m1y2q6g9 --db-url=mysql://robert:m1y2q6g9@davidwhitman.c0ceutlvokql.us-east-1.rds.amazonaws.com/drupal
sudo chmod -R 2775 sites/default/files
sudo chown -R ec2-user sites/default/files
sudo chgrp -R apache sites/default/files
vi .htaccess
# Add the next three lines under `RewriteEngine on`, and uncomment www redirect to force all traffic to https://www.[DOMAIN].com
RewriteCond %{HTTPS} off
RewriteCond %{HTTP:X-Forwarded-Proto} !https
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
# End Drupal 7 Installation and Configuration


drush dis -y overlay toolbar comment
drush pm-uninstall -y overlay toolbar comment
drush dl admin_menu date views token transliteration multiform entity entityreference features rules devel module_filter wysiwyg media-7.x-2.19 file_entity email entityreference_autocomplete fontawesome fontyourface honeypot htmlpurifier imagecache_token libraries link metatag plupload s3fs addressfield pathauto ctools twilio
drush dl omega-7.x-4.4 adminimal_theme-7.x-1.x-dev

# ADD LIBRARIES FOLDER - awssdk2, blueimp, ckeditor, colorbox, facebook-php-sdk, fontawesome, htmlpurifier, plupload, twilio, wvega-timepicker
drush en -y admin_menu admin_menu_toolbar multiform date_api date date_popup date_views views views_ui token transliteration entity entityreference entity_token rules devel module_filter wysiwyg media media_internet media_wysiwyg file_entity email entityreference_autocomplete fontawesome fontyourface fontyourface_ui local_fonts google_fonts_api honeypot htmlpurifier imagecache_token libraries link metatag plupload s3fs dblog addressfield metatag_views pathauto rules_admin fontyourface_wysiwyg twilio

cd /var/www/html/sites/all/themes/[YOUR_OMEGA_SUBTHEME]
npm update
npm install --save-dev del gulp gulp-plumber gulp-sass gulp-watch gulp-autoprefixer gulp-debug gulp-css-globbing gulp-livereload gulp-order gulp-sourcemaps gulp-svg2png gulp-newer gulp-size node-sass-globbing gulp-cssmin
find node_modules -type f -name '*.info' | xargs rm
gem install singularity
gem install compass
gem install breakpoint
npm install jquery-colpick --save
npm install -g gulp gulp-util compass breakpoint singularity gulp-sass-glob
gem update


// OPTIONAL - INSTALL JAVA JRE AND JDK AND APACHE SOLR VERSION 6.6.2
sudo yum install java-1.8.0-openjdk
sudo yum install java-1.8.0-openjdk-devel
cd /tmp
wget http://www.us.apache.org/dist/lucene/solr/6.6.2/solr-6.6.2.tgz
tar xzf solr-6.6.2.tgz solr-6.6.2/bin/install_solr_service.sh --strip-components=2
sudo ./install_solr_service.sh solr-6.6.2.tgz
sudo service solr status
sudo su - solr -c "/opt/solr/bin/solr create -c drupal -n data_driven_schema_configs"
drush dl search_api facetapi search_api_solr
sudo cp sites/all/modules/contrib/search_api_solr/solr-conf/6.x/*.* /var/solr/data/drupal/conf/
Collapse



Drupal8EC2Setup.sh 
#!/bin/bash

# Begin Initial Server Configuration Setup
sudo yum update -y
sudo yum install -y httpd24 gcc git mod24_ssl
sudo yum install -y mysql57 mysql57-server
sudo yum install -y php71 php71-cli php71-devel php71-fpm php71-gd php71-imap php71-json php71-mbstring php71-mcrypt php71-mysqlnd php71-opcache php71-pdo php71-pecl-apcu php71-xml php71-xmlrpc
sudo yum groupinstall "Development Tools" -y
sudo service httpd start
sudo chkconfig httpd on
chkconfig --list httpd
sudo usermod -a -G apache ec2-user
sudo service httpd restart
sudo chown -R ec2-user:apache /var/www
sudo chmod 2775 /var/www
find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;
sudo service httpd restart
sudo service mysqld start --explicit_defaults_for_timestamp
mysqladmin -u root password 'm1y2q6g9'
sudo chkconfig mysqld on
mysql -u root -p"m1y2q6g9" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
mysql -u root -p"m1y2q6g9" -e "DELETE FROM mysql.user WHERE User=''"
mysql -u root -p"m1y2q6g9" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
mysql -u root -p"m1y2q6g9" -e "FLUSH PRIVILEGES"
sudo service httpd restart
# End Initial Server Configuration Setup

# Setup SSL via Certbot
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
sudo ./certbot-auto --debug -v --server https://acme-v01.api.letsencrypt.org/directory certonly -d robertblack.me -d www.robertblack.me

sudo nano /etc/httpd/conf.d/ssl.conf

CERTIFICATE PATHS
	/etc/letsencrypt/live/robertblack.me/cert.pem - Certificate
	/etc/letsencrypt/live/robertblack.me/fullchain.pem - Full Chain
	/etc/letsencrypt/live/robertblack.me/privkey.pem - Private Key

sudo service httpd restart

sudo nano /etc/crontab
39      1,13    *       *       *       root    /home/ec2-user/certbot-auto renew --no-self-upgrade --debug

sudo service crond restart
# End Certbot Setup -- Exit and Reconnect via SSH.

# Ruby and RVM Installation
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable --ruby
source /home/ec2-user/.rvm/scripts/rvm
rvm install "ruby-2.5.0"
source /home/ec2-user/.rvm/scripts/rvm
rvm --default use ruby-2.5.0
# End Ruby and RVM Installation

# Nodejs and NVM Installation -- `nvm install --lts` will install the latest version
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 8.11.1
nvm alias default v8.11.1
# End Nodejs and NVM Installation

# Begin PECL Uploadprogress Installation
sudo su
cd ~/
git clone https://git.php.net/repository/pecl/php/uploadprogress.git
cd uploadprogress
/usr/bin/phpize
./configure
make
sudo make install

vi /etc/php.d/20-uploadprogress.so
# ADD THE FOLLOWING TO END OF FILE
; Enable Upload Progress
extension=uploadprogress.so

apachectl -t
apachectl restart

vi /etc/php.ini
# ADD THE FOLLOWING TO END OF FILE
extension=uploadprogress.so

sudo apachectl restart
# End PECL Uploadprogress Installation

# Begin Composer Installation (Global)
cd /var/www
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
# End Composer Installation

# Begin Drupal 8 Download via Composer
composer create-project drupal-composer/drupal-project:8.x-dev html --stability dev --no-interaction
# End Drupal 8 Download via Composer

# Require Drush Globally
cd ~/
touch .bash_profile
nano .bash_profile # Add the next line to bottom
export PATH="$HOME/.composer/vendor/bin:$PATH"
cd /var/www/html
composer global require drush/drush:dev-master
composer global require drush/drush:8.*

# Edit Default Webroot Location
sudo vi /etc/httpd/conf/httpd.conf # Change DocumentRoot to "/var/www/html/web" and set `AllowOverride` to ALL under "/var/www/html"

# Begin Drupal 8 Installation via Drush - `drush si standard --db-url=mysql://[db_user]:[db_pass]@[ip-address]/[db_name]`
# End Drupal 8 Installation

# Begin .htaccess Updates - Add under `RewriteEngine on` and uncomment www redirect
RewriteCond %{HTTPS} off
RewriteCond %{HTTP:X-Forwarded-Proto} !https
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
# End .htaccess Updates

# Begin Initial Drupal 8 Configuration and Contributed Module Downloads
cd /var/www/html/web/sites/default
chmod 777 settings.php
vi settings.php # Add next line to bottom of the file
$settings['trusted_host_patterns'] = [ '.*' ];
chmod 444 settings.php
composer require "drupal/module_filter" "drupal/token" "drupal/admin_toolbar" "drupal/search_api" "drupal/bootstrap" "drupal/examples" "drupal/file_entity" "drupal/libraries"
mkdir /var/www/html/web/modules/custom
# End Initial Drupal 8 Configuration and Contributed Module Downloads

# Enable and Setup Bootstrap Theme and Sub-Theme
cd /var/www/html/web/themes/contrib/bootstrap
drush then bootstrap # `drush theme enable` command
mkdir /var/www/html/web/themes/custom
cp -R starterkits/cdn /var/www/html/web/themes/custom/robertblack
cd /var/www/html/web/themes/custom/robertblack
mv THEMENAME.libraries.yml robertblack.libraries.yml
mv THEMENAME.starterkit.yml robertblack.info.yml
mv THEMENAME.theme robertblack.theme
mv config/schema/THEMENAME.schema.yml config/schema/robertblack.schema.yml
mv config/install/THEMENAME.settings.yml config/install/robertblack.settings.yml
vi robertblack.info.yml # Edit Sub-Theme Title, Etc.
vi config/schema/robertblack.schema.yml # Edit Sub-Theme Label, Etc.
drush then robertblack
drush config-set system.theme default robertblack
drush thun bartik
# End Enabling and Setup of Bootstrap Theme and Sub-Theme