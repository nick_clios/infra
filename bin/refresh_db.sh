#!/bin/bash

read -p "Are you sure? " -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
	cd /tmp
	rm ems-db-latest.sql.gz* | true
	aws s3 cp s3://clios-archives/latest-dbs/ems-db-latest.sql.gz .
	gunzip ems-db-latest.sql.gz
	mysql ems_remote < ems-db-latest.sql
fi