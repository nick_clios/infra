#!/bin/bash
# This script is meant to be invoked in a cronjob to continously run our infra playbooks
cd /www/clioawards/clioawards
git checkout master -q
git pull -q
cd /home/centos/network-infra-playbooks
git checkout master -q
git pull -q
flock -n /tmp/ansible-playbook.lock ansible-playbook --vault-password-file .vaultpass -i inventories/$(cat .ci_environment)/hosts site.yml


cd /www/clioawards/clioawards/ 
git status
sudo git fetch 
sudo git checkout dev
sudo git pull
cd ~/bin/
./refresh_db.sh
cd /www/clioawards/clioawards
sudo service solr start
drush dl registry_rebuild
drush rr
drush cron -y
drush updb -y
drush cc all -y
drush sapi-l