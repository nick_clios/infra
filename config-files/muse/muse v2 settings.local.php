<?php
if (is_file("/www/env.inc")) {
  include "/www/env.inc";
  $databases = array (
    'default' =>
    array (
      'default' => array (
        'driver' => 'mysql',
        'database' => $_ENV['RDS_DB'],
        'username' => $_ENV['RDS_USER'],
        'password' => $_ENV['RDS_PASS'],
        'host' => $_ENV['RDS_HOST'],
        'port' => $_ENV['RDS_PORT'],
        'prefix' => '',
      ),
    ),
  );


$CACHE_HOST = $_ENV['CACHE_HOST'] . ':' .  $_ENV['CACHE_PORT'];
if (!drupal_installation_attempted()) {
  $settings['cache']['default'] = 'cache.backend.memcache';
  $settings['memcache_storage']['key_prefix'] = 'cbits';
  $settings['memcache_storage']['memcached_servers'] =  [$CACHE_HOST => 'default'];
}

// Enables to display total hits and misses
# $settings['memcache_storage']['debug'] = TRUE;

$conf = array(
  'cache' => array('default' => 'cache.backend.memcache'),
  'cache_backends' => array ('sites/all/modules/contrib/memcache/memcache.inc'),
  'cache_default_class' => 'MemCacheDrupal',
  'cache_class_cache_form' => 'DrupalDatabaseCache',
  'memcache_key_prefix' => 'cbits',
  'memcache_servers' => array ( $CACHE_HOST => 'default'),
  'memcache_bins' => array('cache' => 'default'),
  'page_cache_without_database' => TRUE,
  'page_cache_invoke_hooks' => FALSE,
);
$settings['cache']['default'] = 'cache.backend.memcache_storage';

$conf['awssdk2_access_key'] = $_ENV['S3_ACCESS'];
$conf['awssdk2_secret_key'] = $_ENV['S3_SECRET'];

$settings['s3fs.access_key'] = 'AKIAJTWC3HUXWI2MK4VA';
$settings['s3fs.secret_key'] = '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';

$config['s3fs.settings']['bucket'] = $_ENV['S3_BUCKET'];

$settings['s3fs.use_s3_for_public'] = TRUE;
$settings['s3fs.use_s3_for_private'] = TRUE;

}

$settings['trusted_host_patterns'] = array(
   '^musebycl\.io$',
   '^.+\.musebycl\.io$',
);

$settings['hash_salt'] = "j9zReFrGwYEVwA2M7Pq1ZsvIjMCjgbtQJ_QJIdiWVLk7RKnmrQ9UrQ8DKLdBB8SXn2LU7E0jZg";