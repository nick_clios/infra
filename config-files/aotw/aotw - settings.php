ec2-user@AOTW-web-prod /www/aotw $ cat aotw/sites/default/settings.php
<?php
if (is_file("/www/env.inc")) {
  include "/www/env.inc";
  $databases = array (
    'default' =>
    array (
      'default' => array (
        'driver' => 'mysql',
        'database' => $_ENV['RDS_DB'],
        'username' => $_ENV['RDS_USER'],
        'password' => $_ENV['RDS_PASS'],
        'host' => $_ENV['RDS_HOST'],
        'port' => $_ENV['RDS_PORT'],
        'prefix' => '',
      ),
    ),
  );

  $CACHE_HOST = $_ENV['CACHE_HOST'] . ':' .  $_ENV['CACHE_PORT'];
  $conf = array(
    'cache_backends' => array ('sites/all/modules/contrib/memcache/memcache.inc'),
    'cache_default_class' => 'MemCacheDrupal',
    'memcache_key_prefix' => 'cbits',
    'memcache_servers' => array ( $CACHE_HOST => 'default'),
    'memcache_bins' => array(
      'cache' => 'default',
    ),
  );
  $conf['https'] = TRUE;
  $_SERVER['HTTPS'] = 'on';

  $conf['sphinx_search_options'] = array(
    'base_directory' => '/tmp',
    'host' => $_ENV['SPHINX_HOST'],
    'port' => 9306,
    'searchd' => '/bin/true',
    'timezone' => 'America/New_York',
    'rt_flush_period' => 3600,
    'max_children' => 50,
    'max_matches' => 1000,
    'collation_server' => 'utf8_general_ci',
    'expansion_limit' => 16,
  );
}
else {
  $local_settings = dirname(__FILE__) . '/settings.local.php';
  if (file_exists($local_settings)) {
    include $local_settings;
  }
}

$update_free_access = FALSE;

$drupal_hash_salt = 'b384a8042736a620269a87b1400bf1b3f3d3863b690daf3d4ecb30fc8cd665b0';



ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);


$conf['s3fs_bucket'] = $_ENV['S3_BUCKET'];
$conf['awssdk2_access_key'] = $_ENV['S3_ACCESS'];
$conf['awssdk2_secret_key'] = $_ENV['S3_SECRET'];
$conf['awssdk2_default_cache_config'] = '/www/cache/';
$conf['s3fs_root_folder'] = '';
$conf['s3fs_use_s3_for_public'] = FALSE;
$conf['s3fs_ignore_cache'] = FALSE;
$conf['s3fs_use_https'] = TRUE;
$conf['search_api_sphinx_manual_handling']=true;

$conf['file_temporary_path'] = '/www/tmp_media';
//$conf['plupload_temporary_uri'] = '/www/tmp_media/';

$conf['s3fs_use_cname'] = TRUE;
$conf['s3fs_domain'] = 'd3nuqriibqh3vw.cloudfront.net';

$base_url = 'https://www.adsoftheworld.com';